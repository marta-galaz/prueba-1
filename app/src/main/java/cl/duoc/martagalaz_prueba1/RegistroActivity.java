package cl.duoc.martagalaz_prueba1;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import cl.duoc.martagalaz_prueba1.BD.BaseDeDatos;
import cl.duoc.martagalaz_prueba1.Clases.Usuario;

public class RegistroActivity extends AppCompatActivity implements View.OnClickListener  {

    EditText etUsuario, etClave, eteRepetirClave;
    Button btnCrearUsuario, btnvolverLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro);


        etUsuario = (EditText)findViewById(R.id.etUsuario);
        etClave = (EditText)findViewById(R.id.etClave);
        eteRepetirClave = (EditText)findViewById(R.id.etRepetirClave);
        btnCrearUsuario = (Button) findViewById(R.id.btnCrearUsuario);
        btnvolverLogin = (Button) findViewById(R.id.btnVolver);

        btnCrearUsuario.setOnClickListener(this);
        btnvolverLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.btnRegistrarse) {
            String mensajeError = "";
            {

                if (etUsuario.getText().toString().length()<0){
                    mensajeError+=" Error Usuario \n";
                }
                if (etClave.getText().toString().length()<0){
                    mensajeError+=" Nombre \n";
                }
                if (eteRepetirClave.getText().toString().length()<0){
                    mensajeError+=" Apellido \n";
                }
                if (!etClave.getText().toString().equals(eteRepetirClave.getText().toString())) {
                    mensajeError += " Contraseña Incorrecta \n";
                }
                else{
                    Usuario u = new Usuario();
                    u.setUsuario(etUsuario.getText().toString());
                    u.setClave(etUsuario.getText().toString());

                    BaseDeDatos.agregarUsuario(u);
                    Toast.makeText(this, "Usuario Registrado", Toast.LENGTH_SHORT).show();
                }
                Toast.makeText(this, mensajeError, Toast.LENGTH_SHORT).show();


            }
        } else if (v.getId() == R.id.btnVolver) {
            Intent i = new Intent(RegistroActivity.this,LoginActivity.class);
            startActivity(i);
        }

    }
}
