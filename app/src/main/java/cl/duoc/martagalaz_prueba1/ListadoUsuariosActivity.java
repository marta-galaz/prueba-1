package cl.duoc.martagalaz_prueba1;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;

import java.util.ArrayList;

import cl.duoc.martagalaz_prueba1.BD.BaseDeDatos;
import cl.duoc.martagalaz_prueba1.Clases.Usuario;

public class ListadoUsuariosActivity extends AppCompatActivity {

    private ListView lbView;
    private ArrayList<Usuario> dataSource;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listado_usuarios);
        lbView =(ListView) findViewById(R.id.tvListadi) ;

        ListadoUsuariosActivity adaptador = new ListadoUsuariosActivity(this,getDataSource());
        lbView.setAdapter(adaptador);


    }
    private ArrayList<Usuario> getDataSource(){



        return BaseDeDatos.obtieneListadoUsuarios();
    }
}
