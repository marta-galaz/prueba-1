package cl.duoc.martagalaz_prueba1.Clases;

/**
 * Created by DUOC on 21-04-2017.
 */

public class Usuario {

    private String usuario;
    private String clave;

    @Override
    public String toString() {
        return "Usuario{" +
                "usuario='" + usuario + '\'' +
                ", contraseña='" + clave + '\'' +
                '}';
    }

    public Usuario() {
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getClave() {
        return clave;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }
}
