package cl.duoc.martagalaz_prueba1;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import cl.duoc.martagalaz_prueba1.Clases.Usuario;

import static java.security.AccessController.getContext;

/**
 * Created by DUOC on 21-04-2017.
 */

public class UsuarioAdapter extends ArrayAdapter<Usuario>{

    private ArrayList<Usuario> dataSource;

    public UsuarioAdapter(Context context, ArrayList<Usuario> dataSource){
        super(context,R.layout.item_usuario,dataSource);
        this.dataSource=dataSource;
    }
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(getContext());
        View item = inflater.inflate(R.layout.item_usuario, null);


        TextView tveTitulo=(TextView)  item.findViewById(R.id.etUsuario);
        tveTitulo.setText(dataSource.get(position).getUsuario());



        return(item);
    }
}
