package cl.duoc.martagalaz_prueba1.BD;

import java.util.ArrayList;

import cl.duoc.martagalaz_prueba1.Clases.Usuario;

/**
 * Created by DUOC on 21-04-2017.
 */

public class BaseDeDatos {

    private static ArrayList<Usuario> values = new ArrayList<>();
    public static void agregarUsuario(Usuario usuario){
        values.add(usuario);
    }

    public static ArrayList<Usuario> obtieneListadoUsuarios(){
        return values;
    }
}
