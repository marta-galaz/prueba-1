package cl.duoc.martagalaz_prueba1;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import cl.duoc.martagalaz_prueba1.BD.BaseDeDatos;
import cl.duoc.martagalaz_prueba1.Clases.Usuario;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    EditText etUsuario, etClave;
    Button btnEntrar, btnRegistrarse;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);


        etUsuario = (EditText)findViewById(R.id.etUsuario);
        etClave = (EditText)findViewById(R.id.etClave);
        btnEntrar = (Button) findViewById(R.id.btnEntrar);
        btnRegistrarse = (Button) findViewById(R.id.btnRegistrarse);


        btnEntrar.setOnClickListener(this);
        btnRegistrarse.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.btnEntrar) {
            for (Usuario u : BaseDeDatos.obtieneListadoUsuarios()) {

                if (u.getUsuario().equals(etUsuario.getText().toString())
                        && u.getClave().equals(etClave.getText().toString())) {
                    Toast.makeText(LoginActivity.this, "Usuario Válido", Toast.LENGTH_SHORT).show();
                    Intent it = new Intent(LoginActivity.this, ListadoUsuariosActivity.class);
                    startActivity(it);
                }
            }
            Toast.makeText(LoginActivity.this, "Usuario no Válido", Toast.LENGTH_SHORT).show();
        } else if (v.getId() == R.id.btnRegistrarse) {
            Intent i = new Intent(LoginActivity.this, RegistroActivity.class);
            startActivity(i);
        }

    }
}
